def menor_nome(nomes):
    menorNome = nomes[0].strip().capitalize()
    for i in nomes:
        if( len(i.strip()) < len(menorNome)):
            menorNome = i.strip().capitalize()
    return menorNome

def main():
    print(menor_nome(['maria', 'josé', 'PAULO', 'Catarina']))
    print(menor_nome(['maria', ' josé  ', '  PAULO', 'Catarina  ']))
    print(menor_nome(['Bárbara', 'JOSÉ  ', 'Bill']))

main()