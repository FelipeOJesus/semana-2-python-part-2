def conta_letras(frase, contar="vogais"):
    if(contar == "vogais"):
        if(frase.lower().count("a") or frase.lower().count("e") or frase.lower().count("i") or frase.lower().count("o") or frase.lower().count("u")):
            return frase.lower().count("a") + frase.lower().count("e") + frase.lower().count("i") + frase.lower().count("o") + frase.lower().count("u")
    return len(frase.replace(" ","")) - (frase.lower().count("a") + frase.lower().count("e") + frase.lower().count("i") + frase.lower().count("o") + frase.lower().count("u"))

def main():
    print(conta_letras('programamos em python'))
    print(conta_letras('programamos em python', 'vogais'))
    print(conta_letras('programamos em python', 'consoantes'))

main()