def primeiro_lex(lista):
    menorLexicografica = lista[0]

    for i in lista:
        if(menorLexicografica > i):
            menorLexicografica = i

    return menorLexicografica

def main():
    print(primeiro_lex(['oĺá', 'A', 'a', 'casa']))
    print(primeiro_lex(['AAAAAA', 'b']))

main()